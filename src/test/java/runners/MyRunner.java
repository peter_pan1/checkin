package runners;


import io.cucumber.java.After;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    plugin = {"html:reports/cucumber-html-report", "json:reports/cucumber.json", "pretty"},
    tags = {"@prod"},
    features = {"src/test/resources"},
    monochrome = true,
    dryRun = false,
    strict = true,
    glue = {"bindings.config.Steps"})
public class MyRunner {


  @BeforeClass
  public static void mock() {

      System.setProperty("browser.type","grid");
     System.setProperty("headless","false");

  }









}