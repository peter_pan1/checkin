package bindings.config.Steps;



import bindings.config.DriverFactory.DriverFactory;
import bindings.config.Pages.produkcja.ProdukcjaKonto;
import bindings.config.Pages.produkcja.ShopPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import java.net.MalformedURLException;
import java.sql.SQLException;



public class MyPageSteps {
    private static final Logger LOGGER = LogManager.getLogger(MyPageSteps.class);


    private final DriverFactory driverFactory;
    public final ProdukcjaKonto produkcjaKonto;
    public final ShopPage shopPage;

    public WebDriver driver;


    public MyPageSteps(final  DriverFactory driverFactory, ProdukcjaKonto produkcjaKonto, ShopPage shopPage) {


        this.driverFactory = driverFactory;
        this.produkcjaKonto = produkcjaKonto;
        this.shopPage = shopPage;
    }




    public void afterStep (){
        new DriverFactory().closeDriver("closing driver"); ;
        driverFactory.closeDriver("closing driver");
    }


    public void afterScenario () throws MalformedURLException {
        new DriverFactory().getDriver().quit();
        driverFactory.getDriver().quit();
    }



    @Then("Customer see shoop page")
    public void customerSeeShoopPage() throws MalformedURLException {shopPage.shopTextCheck();
    }

    @When("Customer is opening store homepage")
    public void customerIsOpeningStoreHomepage() throws MalformedURLException {produkcjaKonto.openMojeKontoFakeshop();

    }

    @Then("Customer is registering into Store")
    public void customerIsRegisteringIntoStore() throws InterruptedException {this.produkcjaKonto.registractionProdukcja();

    }

    @And("I login with following hardcoded credentials")
    public void iLoginWithFollowingHardcodedCredentials(DataTable dataTable) {produkcjaKonto.loginProcessFakeshop2(dataTable);

    }
    @And("Customer opens Shop page")
    public void customerOpensShopPage () throws MalformedURLException {
        shopPage.openShopPage();

    }
    @And("Customer is logg off")
    public void customerIsLogOff (){
        produkcjaKonto.wylogowanieUsera();
    }
}
