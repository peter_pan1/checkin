package bindings.config.Steps.RestSteps;

import bindings.config.Api.TestData.CustomerData;
import bindings.config.Api.pojo.CloudStore.Customer.Customer;
import bindings.config.Api.pojo.CloudStore.Customers.Customers;
import bindings.config.Api.pojo.CloudStore.Product.Product;
import bindings.config.Api.pojo.CloudStore.ProductAttribute.ProductAttribute;
import bindings.config.Api.pojo.CloudStore.ProductCategory.ProductCategory;
import bindings.config.Api.pojo.CloudStore.Products.Products;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;

public class RestSteps {

    public static String username = "ck_c31dc257b795f6b8dba03057c92f38e190c9b9c7";
    public static String password = "cs_441d458aa3e41ca4bc99403c00e282a898eec930";
    public String productsEndpoint = "products";
    private String productFuerta = "393";
    private String productCreated = "975";
    private static int port = 80;
    private static String url = "https://testowka.cloudaccess.host/";
    private static String basePath = "index.php/wp-json/wc/v3/";
    public static String customersEndpoint = "customers/";
    protected static String OrdersEndpoint = "orders";
    protected static String ProductEndpoint = "products/" ;
    protected static String ProductAttributesEndpoint = "/products/attributes";
    protected static String ProductCategory = "products/categories";
    protected final String contentType = "application/json";
    public int dane ;



    @Given("Make Secure connection")
    public   void BasicEndpointConfiguraction() {
        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
        baseURI = url;
        //  RestAssured.port = port;
        RestAssured.basePath = basePath;
        authentication = oauth(username, password, "", "");


    }
    @And("POST new Customer")
    public void createCustomer () {
        CustomerData data = new CustomerData();
        Customer cu = data.createCustomer();
      JsonPath jsonPath = given()
                .contentType("application/json")
                .body(cu)
                .when()
                .post(customersEndpoint)
                .then()
                .statusCode(201)
                .extract().jsonPath();
        this.dane = jsonPath.getInt("id");
    }
    @And("POST new Customer2")
    public void createCustomer2 (DataTable dataTable) {
        CustomerData data = new CustomerData();
        Customer cu = data.createCustomer2(dataTable);
        JsonPath jsonPath = given()
                .contentType("application/json")
                .body(cu)
                .when()
                .post(customersEndpoint)
                .then()
                .statusCode(201)
                .extract().jsonPath();
        this.dane = jsonPath.getInt("id");
    }
    @And("Validate data for new registered customer")
    public  void getDataforRegisteredCustomer() {

        given()
                .contentType("application/json")
                .when()
                .get(customersEndpoint +this.dane)
                .then();


    }
    @And("GET customers list from Customer endpoint")
    public void getAllCustomersFromCustomersEndpoint (){
     String response =   given()
                .contentType("application/json")
                .when()
                .get(customersEndpoint)
                .then()
        .extract().path("id").toString();

     System.out.println(response);

    }
    @And("System creates ramdom Product")
    public void postRamdomProduct (){
     //   CustomerData data = new CustomerData ();
        Products product = new Products();
        given()
                .contentType("application/json")
                .body(product.getJsonString(product))
                .when()
                .post(ProductEndpoint)
                .then().log().all()
                .statusCode(201);



   }

    @And("System creates ramdom Product Category")
    public void postRamdomProductCategory (){
        CustomerData data = new CustomerData ();
        bindings.config.Api.pojo.CloudStore.ProductCategory.ProductCategory productCategory = data.createNewProductCategory();
        given()
                .contentType("application/json")
                .body(productCategory)
                .when()
                .post(ProductEndpoint)
                .then()
                .statusCode(201);



    }
    @And("System creates ramdom Product Attrbute")
    public void createProductAttribute (){
     ProductAttribute productAttribute = new ProductAttribute();
        given()
                .contentType("application/json")
                .body(productAttribute)
                .when()
                .post(ProductAttributesEndpoint)
                .then()
                .statusCode(201);



    }

    @And("System creates full Customer")
    public void createFullCustomer (){
        Customers ddd = new Customers();
        given()
                .contentType("application/json")
                .body(ddd)
                .when()
                .post(customersEndpoint)
                .then()
                .statusCode(201);



    }
}