package bindings.config.Api.config.googleStore;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeAll;

import static io.restassured.RestAssured.*;

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class BasicEndpointConfiguraction {
    public static String username = "ck_c31dc257b795f6b8dba03057c92f38e190c9b9c7";
    public static String password = "cs_441d458aa3e41ca4bc99403c00e282a898eec930";
    public String productsEndpoint = "products";
    private String productFuerta = "393";
    private String productCreated = "975";
    private static int port = 80;
    private static String url = "https://testowka.cloudaccess.host/";
    private static String basePath = "index.php/wp-json/wc/v3/";
    public static String customersEndpoint = "customers/";
    protected static String OrdersEndpoint = "orders";
    protected static String ProductCategory = "products/categories";
    protected final String contentType = "application/json";
    protected Response lastresponse ;

    public Response getLastresponse() {
        return lastresponse;
    }

    public void setLastresponse(Response lastresponse) {
        this.lastresponse = lastresponse;
    }
@BeforeAll
    public static void BasicEndpointConfiguraction() {
    RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
        baseURI = url;
      //  RestAssured.port = port;
        RestAssured.basePath = basePath;
        authentication = oauth(username, password, "", "");
    }
}