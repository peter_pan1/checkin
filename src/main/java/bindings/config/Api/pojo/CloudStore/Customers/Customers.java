
package bindings.config.Api.pojo.CloudStore.Customers;


import com.devskiller.jfairy.Fairy;
import com.devskiller.jfairy.producer.person.Person;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@JsonView
public class Customers {

    Fairy fairy = Fairy.create();
    Person person = fairy.person();

    @Expose
    private Billing billing = getBilling();
    @Expose
    private String email =person.getEmail();
    @SerializedName("first_name")
    private String firstName = person.getFirstName();
    @SerializedName("last_name")
    private String lastName = person.getLastName();
    @Expose
    private Shipping shipping ;
    @Expose
    private String username = person.getUsername();
    @Expose
    private String password = "Grudzien2021!" ;

    public Billing getBilling() {
        return billing;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Shipping getShipping() {
        return shipping;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }



    public static class Builder {

        private Billing billing;
        private String email;
        private String firstName;
        private String lastName;
        private Shipping shipping;
        private String username;
        private String password;

        public Customers.Builder withBilling(Billing billing) {
            this.billing = billing;
            return this;
        }

        public Customers.Builder withEmail(String email) {
            this.email = email;
            return this;
        }

        public Customers.Builder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Customers.Builder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Customers.Builder withShipping(Shipping shipping) {
            this.shipping = shipping;
            return this;
        }

        public Customers.Builder withUsername(String username) {
            this.username = username;
            return this;
        }
        public Customers.Builder withPassword (String password){
            this.password =password ;
            return this;
        }

        public Customers build  () {
            Customers customers = new Customers();
            customers.billing = billing;
            customers.email = email;
            customers.firstName = firstName;
            customers.lastName = lastName;
            customers.shipping = shipping;
            customers.username = username;
            customers.password = password;
            return customers;
        }

    }

}
