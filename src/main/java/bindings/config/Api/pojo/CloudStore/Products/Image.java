
package bindings.config.Api.pojo.CloudStore.Products;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@SuppressWarnings("unused")
public class Image {

  @SerializedName("src")
  @Expose
  private String src =
      "http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_front.jpg";

  public String getJsonString(Image image) {
    Gson gson = new GsonBuilder().create();
    return gson.toJson(image);
  }
}
