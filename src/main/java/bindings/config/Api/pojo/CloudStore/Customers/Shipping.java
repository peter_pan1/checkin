
package bindings.config.Api.pojo.CloudStore.Customers;



import com.devskiller.jfairy.Fairy;
import com.devskiller.jfairy.producer.person.Person;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.javafaker.Company;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@SuppressWarnings("unused")
public class Shipping {

    Fairy fairy = Fairy.create();
    Person person = fairy.person();



    @SerializedName("address_1")
    private String address1 = "hubska 56";
    @SerializedName("address_2")
    private String address2 = "Testowa 55";
    @Expose
    private String city = "Wroclaw";
    @Expose
    private String company = "TestoweCompany";
    @Expose
    private String country = "Poland";
    @SerializedName("first_name")
    private String firstName = person.getFirstName();
    @SerializedName("last_name")
    private String lastName = person.getLastName();
    @Expose
    private String postcode = "50-506";
    @Expose
    private String state = "Dolnoslaskie";

    public String getAddress1() {
        return address1;
    }

    public String getAddress2() {
        return address2;
    }

    public String getCity() {
        return city;
    }

    public String getCompany() {
        return company;
    }

    public String getCountry() {
        return country;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPostcode() {
        return postcode;
    }

    public String getState() {
        return state;
    }

    public static class Builder {

        private String address1;
        private String address2;
        private String city;
        private String company;
        private String country;
        private String firstName;
        private String lastName;
        private String postcode;
        private String state;

        public Shipping.Builder withAddress1(String address1) {
            this.address1 = address1;
            return this;
        }

        public Shipping.Builder withAddress2(String address2) {
            this.address2 = address2;
            return this;
        }

        public Shipping.Builder withCity(String city) {
            this.city = city;
            return this;
        }

        public Shipping.Builder withCompany(String company) {
            this.company = company;
            return this;
        }

        public Shipping.Builder withCountry(String country) {
            this.country = country;
            return this;
        }

        public Shipping.Builder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Shipping.Builder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Shipping.Builder withPostcode(String postcode) {
            this.postcode = postcode;
            return this;
        }

        public Shipping.Builder withState(String state) {
            this.state = state;
            return this;
        }

        public Shipping build() {
            Shipping shipping = new Shipping();
            shipping.address1 = address1;
            shipping.address2 = address2;
            shipping.city = city;
            shipping.company = company;
            shipping.country = country;
            shipping.firstName = firstName;
            shipping.lastName = lastName;
            shipping.postcode = postcode;
            shipping.state = state;
            return shipping;
        }

    }

}
