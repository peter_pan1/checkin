
package bindings.config.Api.pojo.CloudStore.Products;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@SuppressWarnings("unused")
public class Products {
    Category category = new Category();

    @SerializedName("categories")
    @Expose
    private List<Category> categories;
    @Expose
    private String description = "esque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit";
    @SerializedName("images")
    @Expose
    private List<Image> images;
    @Expose
    private String name = "Premium Quality";
    @SerializedName("regular_price")
    private String regularPrice ="20.00" ;
    @SerializedName("short_description")
    @Expose
    private String shortDescription ="Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.";
    @Expose
    private String type = "simple";


    public String getJsonString(Products products) {


        Gson gson = new GsonBuilder()

                .setPrettyPrinting()

              //  .excludeFieldsWithoutExposeAnnotation()
                .create();
        return gson.toJson(products);
        //todo
    }
}







