
package bindings.config.Api.pojo.CloudStore.Customers;



import com.devskiller.jfairy.Fairy;
import com.devskiller.jfairy.producer.person.Person;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@SuppressWarnings("unused")
public class Billing {
    Fairy fairy = Fairy.create();
    Person person = fairy.person();
    @SerializedName("address_1")
    private String address1 = "hubska 56";
    @SerializedName("address_2")
    private String address2 = "Testowa 55";
    @Expose
    private String city = "Wroclaw";

    @Expose
    private String company= "TestoweCompany";
    @Expose
    private String country = "Poland";
    @Expose
    private String email = person.getEmail();
    @SerializedName("first_name")
    private String firstName= person.getFirstName();
    @SerializedName("last_name")
    private String lastName = person.getLastName();
    @Expose
    private String phone = "1234567890";
    @Expose
    private String postcode = "50-506";
    @Expose
    private String state = "Dolnoslaskie";

    public String getAddress1() {
        return address1;
    }

    public String getAddress2() {
        return address2;
    }

    public String getCity() {
        return city;
    }

    public String getCompany() {
        return company;
    }

    public String getCountry() {
        return country;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhone() {
        return phone;
    }

    public String getPostcode() {
        return postcode;
    }

    public String getState() {
        return state;
    }

    public static class Builder {

        private String address1;
        private String address2;
        private String city;
        private String company;
        private String country;
        private String email;
        private String firstName;
        private String lastName;
        private String phone;
        private String postcode;
        private String state;

        public Billing.Builder withAddress1(String address1) {
            this.address1 = address1;
            return this;
        }

        public Billing.Builder withAddress2(String address2) {
            this.address2 = address2;
            return this;
        }

        public Billing.Builder withCity(String city) {
            this.city = city;
            return this;
        }

        public Billing.Builder withCompany(String company) {
            this.company = company;
            return this;
        }

        public Billing.Builder withCountry(String country) {
            this.country = country;
            return this;
        }

        public Billing.Builder withEmail(String email) {
            this.email = email;
            return this;
        }

        public Billing.Builder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Billing.Builder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Billing.Builder withPhone(String phone) {
            this.phone = phone;
            return this;
        }

        public Billing.Builder withPostcode(String postcode) {
            this.postcode = postcode;
            return this;
        }

        public Billing.Builder withState(String state) {
            this.state = state;
            return this;
        }

        @Override
        public String toString() {
            return "Billing{" +
                    "address1='" + address1 + '\'' +
                    ", address2='" + address2 + '\'' +
                    ", city='" + city + '\'' +
                    ", company='" + company + '\'' +
                    ", country='" + country + '\'' +
                    ", email='" + email + '\'' +
                    ", firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    ", phone='" + phone + '\'' +
                    ", postcode='" + postcode + '\'' +
                    ", state='" + state + '\'' +
                    '}';
        }

        public Billing build() {
            Billing billing = new Billing();
            billing.address1 = address1;
            billing.address2 = address2;
            billing.city = city;
            billing.company = company;
            billing.country = country;
            billing.email = email;
            billing.firstName = firstName;
            billing.lastName = lastName;
            billing.phone = phone;
            billing.postcode = postcode;
            billing.state = state;
            return billing;
        }


    }

}
