
package bindings.config.Api.pojo.CloudStore.Customer;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;

import javax.annotation.processing.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "email",
        "first_name",
        "last_name",
        "username",
        "password",
        "billing",
        "shipping"
})
public class Customer {
    @SerializedName("password")
    private String password;
    @SerializedName("billing")
    private Billing Billing;
    @SerializedName("email")
    private String Email;
    @SerializedName("first_name")
    private String FirstName;
    @SerializedName("last_name")
    private String LastName;
    @SerializedName("shipping")
    private Shipping Shipping;
    @SerializedName("username")
    private String username;

    public String getPassword() {
        return password;
    }

    public Billing getBilling() {
        return Billing;
    }

    public void setBilling(Billing billing) {
        Billing = billing;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public Shipping getShipping() {
        return Shipping;
    }

    public void setShipping(Shipping shipping) {
        Shipping = shipping;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
