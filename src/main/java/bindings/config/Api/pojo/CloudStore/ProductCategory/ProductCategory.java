
package bindings.config.Api.pojo.CloudStore.ProductCategory;



import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.Expose;

import javax.annotation.processing.Generated;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("net.hexar.json2pojo")
@JsonPropertyOrder({
        "name",
        "image"

})
@SuppressWarnings("unused")
public class ProductCategory {

    @Expose
    private Image image;
    @Expose
    private String name;

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
