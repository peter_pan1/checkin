
package bindings.config.Api.pojo.CloudStore.CustomerResponse;

import java.util.List;

import javax.annotation.processing.Generated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CustomerResponse {

    @Expose
    private bindings.config.Api.pojo.CloudStore.CustomerResponse._links _links;
    @SerializedName("avatar_url")
    private String avatarUrl;
    @Expose
    private Billing billing;
    @SerializedName("date_created")
    private String dateCreated;
    @SerializedName("date_created_gmt")
    private String dateCreatedGmt;
    @SerializedName("date_modified")
    private String dateModified;
    @SerializedName("date_modified_gmt")
    private String dateModifiedGmt;
    @Expose
    private String email;
    @SerializedName("first_name")
    private String firstName;
    @Expose
    private Long id;
    @SerializedName("is_paying_customer")
    private Boolean isPayingCustomer;
    @SerializedName("last_name")
    private String lastName;
    @SerializedName("meta_data")
    private List<Object> metaData;
    @Expose
    private String role;
    @Expose
    private Shipping shipping;
    @Expose
    private String username;

    public bindings.config.Api.pojo.CloudStore.CustomerResponse._links get_links() {
        return _links;
    }

    public void set_links(bindings.config.Api.pojo.CloudStore.CustomerResponse._links _links) {
        this._links = _links;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public Billing getBilling() {
        return billing;
    }

    public void setBilling(Billing billing) {
        this.billing = billing;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDateCreatedGmt() {
        return dateCreatedGmt;
    }

    public void setDateCreatedGmt(String dateCreatedGmt) {
        this.dateCreatedGmt = dateCreatedGmt;
    }

    public String getDateModified() {
        return dateModified;
    }

    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    public String getDateModifiedGmt() {
        return dateModifiedGmt;
    }

    public void setDateModifiedGmt(String dateModifiedGmt) {
        this.dateModifiedGmt = dateModifiedGmt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getIsPayingCustomer() {
        return isPayingCustomer;
    }

    public void setIsPayingCustomer(Boolean isPayingCustomer) {
        this.isPayingCustomer = isPayingCustomer;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Object> getMetaData() {
        return metaData;
    }

    public void setMetaData(List<Object> metaData) {
        this.metaData = metaData;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Shipping getShipping() {
        return shipping;
    }

    public void setShipping(Shipping shipping) {
        this.shipping = shipping;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
