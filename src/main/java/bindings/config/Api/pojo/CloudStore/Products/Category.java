
package bindings.config.Api.pojo.CloudStore.Products;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@SuppressWarnings("unused")
public class Category {

    @Expose
    private Integer id =1;
    public String getJsonString (Category category) {
    Gson gson = new GsonBuilder()
            .create();
    return gson.toJson(category);




    }

}
