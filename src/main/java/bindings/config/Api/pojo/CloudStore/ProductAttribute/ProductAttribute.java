
package bindings.config.Api.pojo.CloudStore.ProductAttribute;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ProductAttribute {

    @SerializedName("has_archives")
    private Boolean hasArchives = true;
    @Expose
    private String name = "Test";
    @SerializedName("order_by")
    private String orderBy = "menu_order";
    @Expose
    private String slug = "pa_color";
    @Expose
    private String type = "select";

    public Boolean getHasArchives() {
        return hasArchives;
    }

    public String getName() {
        return name;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public String getSlug() {
        return slug;
    }

    public String getType() {
        return type;
    }

    public static class Builder {

        private Boolean hasArchives;
        private String name;
        private String orderBy;
        private String slug;
        private String type;

        public ProductAttribute.Builder withHasArchives(Boolean hasArchives) {
            this.hasArchives = hasArchives;
            return this;
        }

        public ProductAttribute.Builder withName(String name) {
            this.name = name;
            return this;
        }

        public ProductAttribute.Builder withOrderBy(String orderBy) {
            this.orderBy = orderBy;
            return this;
        }

        public ProductAttribute.Builder withSlug(String slug) {
            this.slug = slug;
            return this;
        }

        public ProductAttribute.Builder withType(String type) {
            this.type = type;
            return this;
        }

        public ProductAttribute build() {
            ProductAttribute productAttribute = new ProductAttribute();
            productAttribute.hasArchives = hasArchives;
            productAttribute.name = name;
            productAttribute.orderBy = orderBy;
            productAttribute.slug = slug;
            productAttribute.type = type;
            return productAttribute;
        }

    }

}
