package bindings.config.Api.pojo.googleStore.customer2;

import com.devskiller.jfairy.Fairy;
import com.devskiller.jfairy.producer.person.Person;

public class shipping {
    Fairy fairy = Fairy.create();
    Person person = fairy.person();
    private String first_name = person.getFirstName();
    private String last_name = person.getLastName();
    private String company = person.getFirstName();
    private String address_1 = person.getFirstName();
    private String address_2 = person.getFirstName();
    private String city = person.getFirstName();
    private String state = person.getFirstName();
    private String postcode = person.getFirstName();
    private String country = person.getFirstName();

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setAddress_1(String address_1) {
        this.address_1 = address_1;
    }

    public void setAddress_2(String address_2) {
        this.address_2 = address_2;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "shipping{" +
                "first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", company='" + company + '\'' +
                ", address_1='" + address_1 + '\'' +
                ", address_2='" + address_2 + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", postcode='" + postcode + '\'' +
                ", country='" + country + '\'' +
                '}';
    }

    public String getFirst_name() { return first_name; }
    public String getLast_name() { return last_name; }
    public String getCompany() { return company; }
    public String getAddress_1() { return address_1; }
    public String getAddress_2() { return address_2; }
    public String getCity() { return city; }
    public String getState() { return state; }
    public String getPostcode() { return postcode; }
    public String getCountry() { return country; }

}
