package bindings.config.Api.pojo.googleStore.customer;

import com.devskiller.jfairy.Fairy;
import com.devskiller.jfairy.producer.person.Person;
import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "email",
        "first_name",
        "last_name",
        "username",
        "billing",
        "shipping"
})
public class Example implements Serializable
{

    Fairy fairy = Fairy.create();
    Person person = fairy.person();

    @JsonProperty("email")
    private String email =person.getEmail();
    @JsonProperty("first_name")
    private String firstName = person.getFirstName();
    @JsonProperty("last_name")
    private String lastName = person.getLastName();
    @JsonProperty("username")
    private String username = person.getUsername();
    @JsonProperty("billing")
    private String billing =  person.getMiddleName();
    @JsonProperty("shipping")
    private String shipping = person.getMiddleName();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 9178060818134702349L;

    /**
     * No args constructor for use in serialization
     *
     */
    public Example() {
    }

    /**
     *
     * @param firstName
     * @param lastName
     * @param shipping
     * @param email
     * @param username
     * @param billing
     */
    public Example(String email, String firstName, String lastName, String username, String billing, String shipping) {
        super();
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.billing = billing;
        this.shipping = shipping;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    public Example withEmail(String email) {
        this.email = email;
        return this;
    }

    @JsonProperty("first_name")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("first_name")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Example withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    @JsonProperty("last_name")
    public String getLastName() {
        return lastName;
    }

    @JsonProperty("last_name")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Example withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    @JsonProperty("username")
    public String getUsername() {
        return username;
    }

    @JsonProperty("username")
    public void setUsername(String username) {
        this.username = username;
    }

    public Example withUsername(String username) {
        this.username = username;
        return this;
    }

    @JsonProperty("billing")
    public String getBilling() {
        return billing;
    }

    @JsonProperty("billing")
    public void setBilling(String billing) {
        this.billing = billing;
    }

    public Example withBilling(String billing) {
        this.billing = billing;
        return this;
    }

    @JsonProperty("shipping")
    public String getShipping() {
        return shipping;
    }

    @JsonProperty("shipping")
    public void setShipping(String shipping) {
        this.shipping = shipping;
    }

    public Example withShipping(String shipping) {
        this.shipping = shipping;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Example withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("email", email).append("firstName", firstName).append("lastName", lastName).append("username", username).append("billing", billing).append("shipping", shipping).append("additionalProperties", additionalProperties).toString();
    }

}