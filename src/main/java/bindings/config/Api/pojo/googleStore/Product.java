package bindings.config.Api.pojo.googleStore;

import com.devskiller.jfairy.Fairy;
import com.devskiller.jfairy.producer.BaseProducer;
import com.devskiller.jfairy.producer.person.Person;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {
    Fairy fairy = Fairy.create();
    Person person = fairy.person();
    BaseProducer text = fairy.baseProducer();
    private int id = text.randomBetween(0, 300);
    private String name = text.bothify("test");
    private String slug = text.letterify("test");
    private String decription = text.bothify("costam") ;

    public Product(int id, String name, String slug, String decription) {
        this.id = id;
        this.name = name;
        this.slug = slug;
        this.decription = decription;
    }

    public Product() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSlug() {
        return slug;
    }

    public String getDecription() {
        return decription;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public void setDecription(String decription) {
        this.decription = decription;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", slug='" + slug + '\'' +
                ", decription='" + decription + '\'' +
                '}';
    }
}
