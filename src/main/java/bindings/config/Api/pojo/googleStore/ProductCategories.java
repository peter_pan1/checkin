package bindings.config.Api.pojo.googleStore;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductCategories {

    private int id ;
    private String name ;
    private String slug ;
    private String parent ;
    private String display ;

    public ProductCategories(int id, String name, String slug, String parent, String display) {
        this.id = id;
        this.name = name;
        this.slug = slug;
        this.parent = parent;
        this.display = display;
    }

    public ProductCategories() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSlug() {
        return slug;
    }

    public String getParent() {
        return parent;
    }

    public String getDisplay() {
        return display;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    @Override
    public String toString() {
        return "ProductCategories{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", slug='" + slug + '\'' +
                ", parent='" + parent + '\'' +
                ", display='" + display + '\'' +
                '}';
    }
}
