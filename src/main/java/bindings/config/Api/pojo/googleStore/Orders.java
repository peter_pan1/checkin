package bindings.config.Api.pojo.googleStore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Orders {
    private int id ;
    private int parent_id ;
    private int number ;
    private String status ;
    private int customer_id ;

    public Orders(int id, int parent_id, int number, String status, int customer_id) {
        this.id = id;
        this.parent_id = parent_id;
        this.number = number;
        this.status = status;
        this.customer_id = customer_id;
    }

    public int getId() {
        return id;
    }

    public int getParent_id() {
        return parent_id;
    }

    public int getNumber() {
        return number;
    }

    public String getStatus() {
        return status;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public Orders() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "id=" + id +
                ", parent_id=" + parent_id +
                ", number=" + number +
                ", status='" + status + '\'' +
                ", customer_id=" + customer_id +
                '}';
    }
}
