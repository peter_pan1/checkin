package bindings.config.Api.pojo.googleStore.customer;

import com.devskiller.jfairy.Fairy;
import com.devskiller.jfairy.producer.person.Person;
import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "first_name",
        "last_name",
        "company",
        "address_1",
        "address_2",
        "city",
        "state",
        "postcode",
        "country"
})
public class Shipping implements Serializable
{
    Fairy fairy = Fairy.create();
    Person person = fairy.person();


    @JsonProperty("first_name")
    private String firstName = person.getFirstName();
    @JsonProperty("last_name")
    private String lastName= person.getLastName();
    @JsonProperty("company")
    private String company = String.valueOf(person.getCompany());
    @JsonProperty("address_1")
    private String address1 = String.valueOf(person.getAddress());
    @JsonProperty("address_2")
    private String address2 = String.valueOf(person.getAddress());
    @JsonProperty("city")
    private String city =person.getMiddleName();
    @JsonProperty("state")
    private String state =person.getMiddleName();
    @JsonProperty("postcode")
    private String postcode ;
    @JsonProperty("country")
    private String country;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 6420142802333841519L;

    /**
     * No args constructor for use in serialization
     *
     */
    public Shipping() {
    }

    /**
     *
     * @param firstName
     * @param lastName
     * @param country
     * @param address2
     * @param city
     * @param address1
     * @param postcode
     * @param company
     * @param state
     */
    public Shipping(String firstName, String lastName, String company, String address1, String address2, String city, String state, String postcode, String country) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.company = company;
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.state = state;
        this.postcode = postcode;
        this.country = country;
    }

    @JsonProperty("first_name")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("first_name")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Shipping withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    @JsonProperty("last_name")
    public String getLastName() {
        return lastName;
    }

    @JsonProperty("last_name")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Shipping withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    @JsonProperty("company")
    public String getCompany() {
        return company;
    }

    @JsonProperty("company")
    public void setCompany(String company) {
        this.company = company;
    }

    public Shipping withCompany(String company) {
        this.company = company;
        return this;
    }

    @JsonProperty("address_1")
    public String getAddress1() {
        return address1;
    }

    @JsonProperty("address_1")
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public Shipping withAddress1(String address1) {
        this.address1 = address1;
        return this;
    }

    @JsonProperty("address_2")
    public String getAddress2() {
        return address2;
    }

    @JsonProperty("address_2")
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public Shipping withAddress2(String address2) {
        this.address2 = address2;
        return this;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    public Shipping withCity(String city) {
        this.city = city;
        return this;
    }

    @JsonProperty("state")
    public String getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    public Shipping withState(String state) {
        this.state = state;
        return this;
    }

    @JsonProperty("postcode")
    public String getPostcode() {
        return postcode;
    }

    @JsonProperty("postcode")
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public Shipping withPostcode(String postcode) {
        this.postcode = postcode;
        return this;
    }

    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    public Shipping withCountry(String country) {
        this.country = country;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Shipping withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("firstName", firstName).append("lastName", lastName).append("company", company).append("address1", address1).append("address2", address2).append("city", city).append("state", state).append("postcode", postcode).append("country", country).append("additionalProperties", additionalProperties).toString();
    }

}