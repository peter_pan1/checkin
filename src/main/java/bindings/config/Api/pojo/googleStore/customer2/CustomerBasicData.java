package bindings.config.Api.pojo.googleStore.customer2;

import bindings.config.Api.pojo.googleStore.customer.Billing;
import bindings.config.Api.pojo.googleStore.customer.Shipping;
import com.devskiller.jfairy.Fairy;
import com.devskiller.jfairy.producer.person.Person;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)

@JsonInclude(JsonInclude.Include.NON_NULL)

public class CustomerBasicData {
    Fairy fairy = Fairy.create();
    Person person = fairy.person();

    private String email =person.getEmail() ;

    private String first_name = person.getFirstName();

    private String last_name = person.getLastName();

    private  String username  = person.getFirstName();
    private String password = "Grudzien2021!";


    private Billing billing  ;
    private Shipping shipping;

    public java.lang.String getPassword() {
        return password;
    }

    @Override
    public java.lang.String toString() {
        return "CustomerBasicData{" +
                "email='" + email + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public void setPassword(java.lang.String password) {
        this.password = password;
    }

    private Object String;


    public String getUsername() {
        return username;
    }




    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setBilling(Billing billing) {
        this.billing = billing;
    }

    public void setShipping(Shipping shipping) {
        this.shipping = shipping;
    }

    public String getEmail() {
        return email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public Billing getBilling() {
        return billing;
    }

    public Shipping getShipping() {
        return shipping;
    }




 
}