package bindings.config.Api.TestData;

import bindings.config.Api.pojo.CloudStore.Customer.Billing;
import bindings.config.Api.pojo.CloudStore.Customer.Customer;
import bindings.config.Api.pojo.CloudStore.Product.Category;
import bindings.config.Api.pojo.CloudStore.Product.Product;
import bindings.config.Api.pojo.CloudStore.ProductCategory.Image;
import bindings.config.Api.pojo.CloudStore.ProductCategory.ProductCategory;
import com.devskiller.jfairy.Fairy;
import com.devskiller.jfairy.producer.company.Company;
import com.devskiller.jfairy.producer.person.Person;
import io.cucumber.datatable.DataTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class CustomerData {
    public String pict ;


    public Customer createCustomer() {
        Fairy fairy = Fairy.create();
        Person person = fairy.person();
        Billing billing = new Billing();
        billing.setAddress1("test1");
        billing.setAddress2("tes2");
        billing.setCity("Warszawa");
        billing.setLastName("Nazwisko");
        billing.setEmail("temat@wp.pl");
        billing.setState("DOlnoslaskie");
        billing.setPhone("12345678");
        billing.setCompany("DOMAR");
        billing.setFirstName("testowylydzuik");
        billing.setPostcode("50-5006");
        Customer customer = new Customer();
        customer.setEmail(person.getEmail());
        customer.setFirstName(person.getFirstName());
        customer.setLastName(person.getLastName());
        customer.setUsername(person.getUsername());
        customer.setPassword("Grudzien2021");
        customer.setBilling(billing);

        return customer;
    }

    public Customer createCustomer2(final DataTable dataTable) {
        Fairy fairy = Fairy.create();
        List<Map<String,String>> data = dataTable.asMaps(String.class,String.class);
        Person person = fairy.person();
        Customer customer = new Customer();
        customer.setEmail(data.get(0).get("Email"));
        customer.setFirstName(data.get(1).get("FirstName"));
        customer.setLastName(data.get(2).get("LastName"));
        customer.setUsername(data.get(3).get("Username"));
        customer.setPassword("Grudzien2021");

        return customer;
    }
    public Product createNewProduct (){
        Fairy fairy = Fairy.create();
        List<String>  c = new ArrayList<>();
        c.add("test");

        List<Category> categories = new ArrayList<>();



        Company company =fairy.company();
        Product produc = new Product();
        produc.setName(company.getName());
        produc.setType("simple");
        produc.setDescription("test1");
        produc.setShortDescription("test2");
        produc.setRegularPrice("20");
        produc.setCategories(categories);


        return produc;
    }


    public ProductCategory createNewProductCategory (){
        Fairy fairy = Fairy.create();
        Company company =fairy.company();
        ProductCategory productCategory = new ProductCategory();
        Image image = new Image();
        image.setSrc("http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_front.jpg");

        productCategory.setName(company.getName());
        productCategory.setImage(image);


        return  productCategory;
    }


}
