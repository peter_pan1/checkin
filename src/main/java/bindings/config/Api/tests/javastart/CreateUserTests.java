package bindings.config.Api.tests.javastart;

import bindings.config.Api.config.javastart.SuiteTestBase;
import bindings.config.Api.pojo.java.ApiResponse;
import bindings.config.Api.pojo.java.User;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

public class CreateUserTests extends SuiteTestBase {

  @Test
  public void givenUserWhenPostUserThenUserIsCreatedTest() {
    User user = new User();
    user.setId(445);
    user.setUsername("firstuser");
    user.setFirstName("Krzysztof");
    user.setLastName("Kowalski");
    user.setEmail("krzysztof@test.com");
    user.setPassword("password");
    user.setPhone("+123456789");
    user.setUserStatus(123);


      ApiResponse apiResponse = given().contentType("application/json")
              .body(user)
              .when().post("user")
              .then().statusCode(200).extract().as(ApiResponse.class);

      assertEquals(apiResponse.getCode(), Integer.valueOf(200), "Code");
      assertEquals(apiResponse.getType(), "unknown", "Type");
      assertEquals(apiResponse.getMessage(), "445", "Message");
  }


}
