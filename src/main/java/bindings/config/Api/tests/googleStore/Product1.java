package bindings.config.Api.tests.googleStore;

import bindings.config.Api.config.googleStore.BasicEndpointConfiguraction;
import bindings.config.Api.pojo.googleStore.Product;
import io.cucumber.java.en.Given;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;

public class Product1 extends BasicEndpointConfiguraction {
    private static String name = "Test";
    private static String slug = "test2";
    @Test

    public void testDeserializationProduct() {
        Product product = when()
                .get(productsEndpoint + "/393")
                .as(Product.class);
        System.out.println(product.toString());
    }



    @Test
@Given("Create and Verify Product")
    public void testSerializationProduct() {
        Product product = new Product(0,name,slug,"costamcosta");
        given().log().all()
                .contentType("application/json")
                .body(product)
                .when()
                .post(productsEndpoint)
                .then()
                .statusCode(201)
                .and()
                .assertThat()
                .body("name", equalTo("Test"));
        System.out.println(product);

    }
}
