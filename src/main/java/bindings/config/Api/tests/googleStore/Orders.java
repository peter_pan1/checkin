package bindings.config.Api.tests.googleStore;

import bindings.config.Api.config.googleStore.BasicEndpointConfiguraction;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Type;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;

public class Orders extends BasicEndpointConfiguraction {

    public Orders(int i, int i1, int i2, String aNew, int i3) {
        super();
    }

    @Test

    public void testDeserializationOrder() {
        Orders orders = when()
                .get(OrdersEndpoint + "/964")
                .as((Type) bindings.config.Api.pojo.CloudStore.Orders.Orders.class);
        System.out.println(orders.toString());
    }

    @Test
    public void testSerializationOrder() {
        Orders orders = new Orders(0,0,0,"new",0);
        given()
                .contentType("application/json")
                .body(orders)
                .when()
                .post(productsEndpoint)
                .then()
                .statusCode(201)
                .and()
                .assertThat()
                .body("name", equalTo("Serialized object"));
        System.out.println(orders);
    }
}
