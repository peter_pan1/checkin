package bindings.config.Api.tests.googleStore;


import bindings.config.Api.config.googleStore.BasicEndpointConfiguraction;
import bindings.config.Api.pojo.CloudStore.Customer.Customer;

import bindings.config.Api.pojo.googleStore.Customers;
import com.devskiller.jfairy.Fairy;
import com.devskiller.jfairy.producer.person.Person;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Type;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;

public class Customers1 extends BasicEndpointConfiguraction {


@Test
    public void createCustomer (){
        Fairy fairy = Fairy.create();
        Person person = fairy.person();
        Customer c = new Customer() ;
        c.setEmail(person.getEmail());
        c.setFirstName(person.getFirstName());
        c.setLastName(person.getLastName());
        c.setUsername(person.getUsername());
        c.setPassword("Grudzien2021");

        given().log().all()
                .contentType("application/json")
                .body(c)
                .when()
                .post(customersEndpoint)
                .then()
                .statusCode(201);
    }



    @Test
    public void testSerializationCustomer() {
        Customers customer = new Customers("piotr@wp.pl", "test", "test");
        given().log().all()
                .contentType("application/json")
                .body(customer)
                .when()
                .post(customersEndpoint)
                .then()
                .statusCode(201)
                .and()
                .assertThat()
                .body("first_name", equalTo("Grudzien2021!"));
    }




}
