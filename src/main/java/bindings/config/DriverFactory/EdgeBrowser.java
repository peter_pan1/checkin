package bindings.config.DriverFactory;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;

public class EdgeBrowser {
    private static final Logger LOGGER = LogManager.getLogger(EdgeBrowser.class);

    public static EdgeOptions getOptions() {
        EdgeOptions profile = new EdgeOptions();

        final EdgeOptions options = new EdgeOptions();

        return options;
    }

    public static WebDriver setupDriver() {

        LOGGER.info(
                "Chrome webdriver version: {}", WebDriverManager.chromedriver().getDownloadedVersion());

        return new EdgeDriver();
    }
}