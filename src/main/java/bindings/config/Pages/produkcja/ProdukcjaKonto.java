package bindings.config.Pages.produkcja;



import bindings.config.DriverFactory.DriverFactory;
import com.devskiller.jfairy.Fairy;
import com.devskiller.jfairy.producer.person.Person;
import io.cucumber.datatable.DataTable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class ProdukcjaKonto extends DriverFactory {
    public WebDriver driver  = getDriver();
    private static final Logger LOGGER = LogManager.getLogger(ProdukcjaKonto.class);
    private static final String REG_EMAIL_NAME = "reg_email";
    private static final String REG_PASS_NAME = "reg_password";
    private static final String REG_BUTTON_NAME = "register";
    // TODO: 11/06/2020  naprawic selektor do subbmit buttona 
    private static final String LOGIN_USERNAME = "username";
    private static final String PASSWORD_FIELD = "password";

    // TODO: 11/06/2020 naprawic selektor do LOGIN BUTTOn 
    private static final String LOGIN_PASSWORD = "Grudzien2021!";
    private static final String PASSWORD_NAME = "password";
    private static final String CONFIRM_NAME = "confirmpassword";
    private static final String ZALOGUJ_XPATH = "//button[@type='submit']";
    private static final String PASSWORD = "Grudzien2021! ";
    private static final String SHOP_ID = "menu-item-198";

    Fairy fairy = Fairy.create();
    Person person = fairy.person();
   JavascriptExecutor jse = (JavascriptExecutor) driver;
  //  Actions actions = new Actions(getDriver());

    @FindBy(id = REG_EMAIL_NAME)
    public WebElement regEmail;

    @FindBy(id = REG_PASS_NAME)
    public WebElement regPass;

    @FindBy(name = REG_BUTTON_NAME)
    public WebElement regButton;

    @FindBy(id = LOGIN_USERNAME)
    public WebElement loginUsername;

    @FindBy(id = PASSWORD_FIELD)
    public WebElement loginPass;

    @FindBy(name = "login")
    public WebElement loginButton;

    @FindBy(linkText = "Moje konto")
    public WebElement tekst;

    @FindBy(partialLinkText = "Log out")
    public WebElement wylogowanie;

    @FindBy(linkText = "Zarejestruj się")
    public WebElement registerText;

    @FindBy(id ="menu-item-198")
    public WebElement shopPage;


    public ProdukcjaKonto() throws MalformedURLException {
        PageFactory.initElements(driver, this);
    }


    public void openMojeKontoFakeshop() throws MalformedURLException {
        getDriver().get("https://testowka.cloudaccess.host/my-account/");
        LOGGER.debug("Opening PROD Login Page ");
    }



    public void registractionProdukcja() throws InterruptedException {
        LOGGER.debug("Proceed to register process");
        regEmail.sendKeys(person.getEmail());
        LOGGER.debug("Used Email "+ person.getEmail());
     jse.executeScript("scroll(239, 771)");
        LOGGER.debug("Used Password "+ PASSWORD);
        regPass.sendKeys(PASSWORD);
        regButton.click();
        Assert.assertEquals(wylogowanie,wylogowanie);
        LOGGER.debug("User registered");
        LOGGER.debug("Scenario Passed ");
        LOGGER.info("Scenario test data" +" "+"EMAIL:"+"  "+person.getEmail()+" "+"PASSWORD:"+" "+PASSWORD);
         closeDriver("closing driver");
    }


    public void loginProcessFakeshop2(final DataTable dataTable){
        List<Map<String,String>> data = dataTable.asMaps(String.class,String.class);
        LOGGER.debug("Proceed to Login process");
        loginUsername.sendKeys (data.get(0).get("Username"));
        LOGGER.debug("User credentials "+ data.get(0));
        loginPass.sendKeys((data.get(0).get("Password")));
      jse.executeScript("scroll(239, 771)");
        loginButton.click();
        LOGGER.debug(loginButton);
        Assert.assertEquals(wylogowanie,wylogowanie);
        LOGGER.debug("Login process passed");

    }

    public void textcheck() {
        Assert.assertEquals(tekst, tekst);
        System.out.println(tekst);

    }

    public void loggoff (){

    }

    public void wylogowanieButton(){
        Assert.assertEquals(wylogowanie,wylogowanie);
    }

    public void wylogowanieUsera (){
      jse.executeScript("scroll(239, 771)");
        wylogowanie.click();
      Assert.assertEquals(regButton, regButton);
       jse.executeScript("scroll(239, 771)");
        closeDriver("closing driver");

    }
    public  void checkSelfService (){Assert.assertEquals(tekst, tekst);
    }

    public void  clickShopID () {
       Assert.assertEquals(shopPage,shopPage);
        shopPage.click();
    }
}
