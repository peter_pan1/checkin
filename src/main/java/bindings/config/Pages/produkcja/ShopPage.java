package bindings.config.Pages.produkcja;


import bindings.config.DriverFactory.DriverFactory;
import com.devskiller.jfairy.Fairy;
import com.devskiller.jfairy.producer.person.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.net.MalformedURLException;

public class ShopPage extends DriverFactory {

    private static final Logger LOGGER = LogManager.getLogger(ShopPage.class);

    Fairy fairy = Fairy.create();
    Person person = fairy.person();
    JavascriptExecutor jse = (JavascriptExecutor) getDriver();


    @FindBy(xpath ="//a[@href='http://testowka.org/index.php/shop/']")
    public WebElement shopPage;

    @FindBy(linkText = "Shop")
    public WebElement ShopText;
    public ShopPage() throws MalformedURLException {PageFactory.initElements(getDriver(), this);
    }

    public void shopTextCheck () throws MalformedURLException {
        getDriver().quit();

    }
    public void openShopPage() throws MalformedURLException {
        getDriver().get("https://testowka.cloudaccess.host/shop/");
        LOGGER.debug("Opening PROD Shop page ");

    }





}