package bindings.config.Hooks;

import bindings.config.DriverFactory.DriverFactory;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class Hooksteps {

    private static final Logger LOGGER = LogManager.getLogger(Hooksteps.class);



    @Before
    public void logScenarioName(final Scenario scenario) {
        LOGGER.info("Scenario name: {}", scenario.getName());
        LOGGER.info("Cucumber tags: {}", scenario.getSourceTagNames());
        LOGGER.info("Cucumber ID: {}",scenario.getId());
    }


}
